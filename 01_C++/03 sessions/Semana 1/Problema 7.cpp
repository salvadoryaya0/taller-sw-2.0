#include<iostream>
#include<math.h>
#define Gr 6,673

using namespace std;
int main(){
	float masa1,masa2,d,G,F,dcuadrado;
	
	masa1=0;
	masa2=0;
	d=0;
	F=0;
	dcuadrado=0;
	
	cout<<"Ingrese el valor de la masa del primer objeto: (en kg)\n";
	cin>>masa1;
	cout<<"Ingrese el valor de la masa del segundo objeto: (en kg)\n";
	cin>>masa2;
	cout<<"Ingrese la longitud de la sepraración de dichos objetos: (en m) \n";
	cin>>d;
	
	dcuadrado=(float)pow(d, 2);
	F=((float)Gr*(float)masa1*(float)masa2)/(float)dcuadrado;
	
	cout<<"El modulo de la fuerza gravitatoria entre dichos objetos es:  (en 10-8 N)\n"<<F;
	
	return 0;
	

}

