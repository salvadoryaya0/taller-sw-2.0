#include<iostream>

using namespace std;

int main(){
	int desaprobados,aprobadosnotables,sobresalientes,numerototal,numeroaprobados;
	float porcentajedesaprobados,porcentajesobresalientes,porcentajeaprobadosnotables,porcentajeaprobados;
	
	desaprobados=0;
	aprobadosnotables=0;
	sobresalientes=0;
	numerototal=0;
	porcentajedesaprobados=0;
	porcentajesobresalientes=0;
	porcentajeaprobadosnotables=0;
	
	cout<<"Ingrese el numero de desaprobados: \n";
	cin>>desaprobados;
	cout<<"Ingrese el numero de aprobados notables: \n";
	cin>>aprobadosnotables;
	cout<<"Ingrese el numero de sobresalientes: \n";
	cin>>sobresalientes;
	
	numeroaprobados = (float)aprobadosnotables + (float)sobresalientes;
	numerototal = (float)desaprobados + (float)aprobadosnotables + (float)sobresalientes;
	porcentajedesaprobados=((float)desaprobados*100)/(float)numerototal;
	porcentajeaprobadosnotables=((float)aprobadosnotables*100)/(float)numerototal;
	porcentajesobresalientes=((float)sobresalientes*100)/(float)numerototal;
	porcentajeaprobados=((float)numeroaprobados*100)/(float)numerototal;
	
	cout<<"el porcentaje de estudiantes aprobados en la asignatura es :\n"<<porcentajeaprobados;
	cout<<"\nel porcentaje de estudiantes desaprobados en la asignatura es: \n"<<porcentajedesaprobados;
	cout<<"\nel porcentaje de estudiantes aprobados notables en la asignatura es: \n"<<porcentajeaprobadosnotables;
	cout<<"\nel porcentaje de estudiantes sobresalientes en la asignatura es: \n"<<porcentajesobresalientes;
	
	return 0;
	
}
